<?php
require("animal.php");
require("frog.php");
require("Ape.php");

$sheep = new Animal("shaun");
echo "Name:" .$sheep->name ."<br>"; // "shaun"
echo "legs:" .$sheep->legs ."<br>"; // 4
echo "Cold Blooded:" .$sheep->cold_blooded ; // "no"

echo "<br>";
echo "<br>";
$kodok = new Frog("buduk");
echo "Name:" .$kodok->name ."<br>"; // "shaun"
echo "legs:" .$kodok->legs ."<br>"; // 4
echo "Cold Blooded:" .$kodok->cold_blooded ."<br>"; // "no"
echo $kodok->jump("Hop Hop"); 

echo "<br>";
echo "<br>";

$kera = new Apes("kera sakti");
echo "Name:" .$kera->name ."<br>"; // "shaun"
echo "legs:" .$kera->legs ."<br>"; // 4
echo "Cold Blooded:" .$kera->cold_blooded ."<br>" ; // "no"
echo $kera->yell("Auooo"); 

?>