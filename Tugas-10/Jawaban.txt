-----SOAL 1-----
CREATE DATABASE myshop;



-----SOAL 2-----
table users:
CREATE TABLE users (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);

table categories:
CREATE TABLE categories (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

table items;
CREATE TABLE items (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) NOT NULL,
    price INTEGER NOT NULL,
    stock INTEGER NOT NULL,
    category_id INTEGER,
    FOREIGN KEY (category_id) REFERENCES categories(id)
);




-----SOAL 3-----
table users:
INSERT INTO users (name, email, password) VALUES
('John Doe', 'john@doe.com', 'john123'),
('Jane Doe', 'jane@doe.com', 'jenita123');

table categories:
INSERT INTO categories (name) VALUES
('gadget'),
('cloth'),
('men'),
('women'),
('branded');

table items:
INSERT INTO items (name, description, price, stock, category_id) VALUES
('Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1),
('Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2),
('IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);




-----SOAL 4-----
a. Mengambil data users
SELECT id, name, email
FROM users;

b. Mengambil data items
SELECT *
FROM items
WHERE price > 1000000;

c. Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori
FROM items
JOIN categories ON items.category_id = categories.id;




-----SOAL 5-----
UPDATE items
SET price = 2500000
WHERE name = 'Sumsang b50';
